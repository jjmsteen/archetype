#pragma once

#include <cmath>

namespace AT
{
	namespace Maths
	{
		// Smallest value a float can be
		static const float kFloatMin = 0.00001f;

		// A constant representing pi
		static const double kPI = 3.141592653589793238462643383279502884197;

		// Inline functions for converting between radians and degrees
		inline double RadiansToDegrees(double radians) { return radians * (180.0 / kPI); }
		inline double DegreesToRadians(double degrees) { return kPI * (degrees / 180.0); }

		inline float ZeroSmallFloat(float f) { return (fabs(f) < kFloatMin) ? 0.0f : f; }
        
        // Lerping functions
        inline double Lerp(double low, double high, double value) { return low + (high - low) * value; }
	}
}